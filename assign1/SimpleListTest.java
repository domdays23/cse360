/* Date: February 10th 2020
 * Name: Dominik Czerny
 * ID: 1210724915
 * CSE 360
 * Assignment 1
 * SimpleListTest class that is a junit test for the SimpleList.
 * It tests the SimpleList, Add, Remove, Count, ToString, and Search methods
 */

package assign1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SimpleListTest {

	/*
	 * Initializes SimpleList
	 */
	@Test
	void testSimpleList() 
	{
		SimpleList test = new SimpleList();
	}
	
	/*
	 * Tests the add method by adding one, checking if the number exists in the right position.
	 * It then repeats that process. Then it adds 7 more numbers to make the array full and tests
	 * to see if they are in the correct position.
	 * It then adds one more to overflow the array and checks to see if 2 is the last number.
	 */
	@Test
	void testAdd() 
	{
		SimpleList test = new SimpleList();
		test.add(1);
		assertEquals(test.list[0], 1);
		test.add(2);
		assertEquals(test.list[0], 2);
		assertEquals(test.list[1], 1);
		assertEquals(test.list[9], 0);
		test.add(3);
		test.add(4);
		test.add(5);
		test.add(6);
		test.add(7);
		test.add(8);
		test.add(9);
		test.add(10);
		assertEquals(test.list[9], 1);
		test.add(20);
		assertEquals(test.list[9], 2);
	}

	/*
	 * Tests the remove method. Adds one number and checks if it exists in the correct position.
	 * It then removes that number and checks to see if there is a number in that same position.
	 * It then fills the array and removes one number at a time to check if the positions are 
	 * maintained
	 */
	@Test
	void testRemove() 
	{
		SimpleList test = new SimpleList();
		test.add(1);
		assertEquals(test.list[0], 1);
		test.remove(1);
		assertEquals(test.list[0], 0);
		test.add(1);
		test.add(2);
		test.add(3);
		test.add(4);
		test.add(5);
		test.add(6);
		test.add(7);
		test.add(8);
		test.add(9);
		test.add(10);
		assertEquals(test.list[9], 1);
		test.remove(1);
		assertEquals(test.list[9], 0);
		test.remove(3);
		assertEquals(test.list[7], 2);
	}
	
	/*
	 * Tests the count method. Adds one number and then checks if count returns one. Adds 9 more
	 * and checks to see if count is at 10. Then overflows to make sure that count stays at 10 even
	 * after adding another number
	 */
	@Test
	void testCount() 
	{
		SimpleList test = new SimpleList();
		test.add(1);
		assertEquals(test.count, 1);
		test.add(2);
		test.add(3);
		test.add(4);
		test.add(5);
		test.add(6);
		test.add(7);
		test.add(8);
		test.add(9);
		test.add(10);
		assertEquals(test.count, 10);
		test.add(20);
		assertEquals(test.count, 10);
		
	}
	
	/*
	 * Tests ToString to see if it returns the correct string. Adds 10 numbers and compares to see if
	 * the output is the same string
	 */
	@Test
	void testToString() 
	{
		SimpleList test = new SimpleList();
		test.add(1);
		test.add(2);
		test.add(3);
		test.add(4);
		test.add(5);
		test.add(6);
		test.add(7);
		test.add(8);
		test.add(9);
		test.add(10);
		assertEquals(test.toString(), "10 9 8 7 6 5 4 3 2 1 ");
	}
	
	/*
	 * Tests search method. Looks to see if a number that doesnt currently reside in the array returns
	 * -1. Adds 10 numbers and then searches for 3 of their positions
	 */
	@Test
	void testSearch() 
	{
		SimpleList test = new SimpleList();
		assertEquals(test.search(5), -1);
		test.add(1);
		test.add(2);
		test.add(3);
		test.add(4);
		test.add(5);
		test.add(6);
		test.add(7);
		test.add(8);
		test.add(9);
		test.add(10);
		assertEquals(test.search(5), 5);
		assertEquals(test.search(10),0);
		assertEquals(test.search(1),9);
	}

}
