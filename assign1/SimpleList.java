/* February 10th 2020
 * Name: Dominik Czerny
 * ID: 1210724915
 * CSE 360
 * Assignment 1
 * SimpleList class creates methods to add, remove, search, count, and display contents of an array.
 */

package assign1;

public class SimpleList 
{
	int[] list;
	int count;
	
	
	//SimpleList constructor initializes the array to a size of 10 and count to 0
	SimpleList()
	{
		list = new int[10];
		count = 0;
	}
	
	/*
	 * Add is a method that takes in a number to add into the first space of the array
	 * if the array has 10 spots filled already, the new number will be added and the last slot
	 * will fall off.
	 */
	public void add(int number)
	{
		int hold;
		int next = number;
		
		for(int iterate = 0; iterate < 10; iterate++)
		{
			hold = list[iterate];
			list[iterate] = next;
			next = hold;
		}
		
		if(count != 10)
		{
			count++;
		}
	}
	
	/*
	 * remove is a method that takes in a number and uses the search method to see if the number
	 * exists within the array. If it does the array will delete that number and push every number
	 * back one slot.
	 */
	public void remove(int number)
	{	
		int iterate = search(number);
		
		if(iterate != -1)
		{
			while(iterate < 10)
			{
				if(iterate + 1 == 10)
				{
					list[iterate] = 0;
				}
				
				else
				{
					list[iterate] = list[iterate + 1];
				}
				
				iterate++;
			}
		}
	}
	
	/*
	 * Count is a method that returns an int. It counts how many numbers are currently in the array.
	 */
	public int count()
	{
		return count;
	}
	
	public String toString()
	{
		String s = "";
		
		for(int iterate = 0; iterate < 10; iterate++)
		{
			s += list[iterate] + " ";
		}
		return s;
	}
	
	/*
	 * Search is a method that takes in a number and searches to see if it is in the array.
	 * If the number is found the method returns the position, if not it returns -1
	 */
	public int search(int number)
	{
		int position = -1;
		
		for(int iterate = 0; iterate<10; iterate++)
		{
			if(list[iterate] == number)
			{
				 position = iterate;
			}
		}
		
		return position;
	}
}