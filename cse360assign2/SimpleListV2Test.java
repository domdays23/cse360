/* February 27th 2020
 * Name: Dominik Czerny
 * ID: 1210724915
 * CSE 360
 * Assignment 2
 * SimpleListV2 Test class that individually tests the methods that add, remove, search, count, 
 * append, first, last, size, and display contents of an array.
 */

package cse360assign2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import assign1.SimpleList;

class SimpleListV2Test {

	/*
	 * Initializes SimpleList
	 */
	@Test
	void testSimpleList() 
	{
		SimpleListV2 test = new SimpleListV2();
	}
	
	/*
	 * Tests the add method by adding one, checking if the number exists in the right position.
	 * It then repeats that process. Then it adds 7 more numbers to make the array full and tests
	 * to see if they are in the correct position.
	 * It then adds one more to overflow the array and checks to see if 20 causes the method to
	 * add 50% more space.
	 */
	@Test
	void testAdd() 
	{
		SimpleListV2 test = new SimpleListV2();
		test.add(1);
		assertEquals(test.list[0], 1);
		test.add(2);
		assertEquals(test.list[0], 2);
		assertEquals(test.list[1], 1);
		assertEquals(test.list[9], 0);
		test.add(3);
		test.add(4);
		test.add(5);
		test.add(6);
		test.add(7);
		test.add(8);
		test.add(9);
		test.add(10);
		assertEquals(test.list[9], 1);
		test.add(20);
		assertEquals(test.list[0], 20);
		assertEquals(test.list[14], 0);
		
	}

	/*
	 * Tests the remove method. Adds one number and checks if it exists in the correct position.
	 * It then removes that number and checks to see if there is a number in that same position.
	 * It then fills the array and removes one number to see if the array has its storage size
	 * reduced by 25%.
	 */
	@Test
	void testRemove() 
	{
		SimpleListV2 test = new SimpleListV2();
		test.add(1);
		test.add(2);
		test.add(3);
		test.add(4);
		assertEquals(test.list[0], 4);
		test.remove(4);
		assertEquals(test.list[0], 3);
		test.add(4);
		test.add(5);
		test.add(6);
		test.add(7);
		test.add(8);
		test.add(9);
		test.add(10);
		test.add(11);
		test.add(12);
		test.add(13);
		test.add(14);
		test.add(15);
		test.add(16);
		test.remove(12);
		assertEquals(test.list[16], 0);
	}
	
	/*
	 * Tests the count method. Adds one number and then checks if count returns one. Adds 9 more
	 * and checks to see if count is at 10. Then overflows to make sure that count continues after
	 * the array expands by 50%. It then removes one number to see if count is reduced.
	 */
	@Test
	void testCount() 
	{
		SimpleListV2 test = new SimpleListV2();
		test.add(1);
		assertEquals(test.count, 1);
		test.add(2);
		test.add(3);
		test.add(4);
		test.add(5);
		test.add(6);
		test.add(7);
		test.add(8);
		test.add(9);
		test.add(10);
		assertEquals(test.count, 10);
		test.add(20);
		assertEquals(test.count, 11);
		test.remove(20);
		assertEquals(test.count, 10);
		
	}
	
	/*
	 * Tests ToString to see if it returns the correct string. Adds 10 numbers and compares to see if
	 * the output is the same string
	 */
	@Test
	void testToString() 
	{
		SimpleListV2 test = new SimpleListV2();
		test.add(1);
		test.add(2);
		test.add(3);
		test.add(4);
		test.add(5);
		test.add(6);
		test.add(7);
		test.add(8);
		test.add(9);
		test.add(10);
		assertEquals(test.toString(), "10 9 8 7 6 5 4 3 2 1 ");
	}
	
	/*
	 * Tests search method. Looks to see if a number that doesnt currently reside in the array returns
	 * -1. Adds 10 numbers and then searches for 3 of their positions
	 */
	@Test
	void testSearch() 
	{
		SimpleListV2 test = new SimpleListV2();
		assertEquals(test.search(5), -1);
		test.add(1);
		test.add(2);
		test.add(3);
		test.add(4);
		test.add(5);
		test.add(6);
		test.add(7);
		test.add(8);
		test.add(9);
		test.add(10);
		assertEquals(test.search(5), 5);
		assertEquals(test.search(10),0);
		assertEquals(test.search(1),9);
	}
	
	/*
	 * Tests Append method by adding 8 numbers normally and then appending 9 to see if 9 is at
	 * the end of the list. It then appends two more numbers to see if the size of the array
	 * increases by 50%.
	 */
	@Test
	void testAppend()
	{
		SimpleListV2 test = new SimpleListV2();
		test.add(1);
		test.add(2);
		test.add(3);
		test.add(4);
		test.add(5);
		test.add(6);
		test.add(7);
		test.add(8);
		test.append(9);
		assertEquals(test.list[8], 9);
		test.append(10);
		test.append(11);
		assertEquals(test.list[10], 11);
	}
	
	/*
	 * Tests First method by adding 1 number and checking if its in the first position, adding another
	 * and checking again, then removes that number to see if the initial number is in the first
	 * position.
	 */
	@Test
	void testFirst()
	{
		SimpleListV2 test = new SimpleListV2();
		test.add(1);
		assertEquals(test.first(), 1);
		test.add(2);
		assertEquals(test.first(), 2);
		test.remove(2);
		assertEquals(test.first(), 1);
	}
	
	/*
	 * Tests Last method by adding 1 number and checking if it is in the last position. It then
	 * adds another number and checks if 1 is still in the last position after being moved.
	 * It then removes the number that was previously added and checks to see if 1 is still in the
	 * last position.
	 */
	@Test
	void testLast()
	{
		SimpleListV2 test = new SimpleListV2();
		test.add(1);
		assertEquals(test.last(), 1);
		test.add(2);
		assertEquals(test.last(), 1);
		assertEquals(test.first(), 2);
		test.remove(2);
		assertEquals(test.last(), 1);
		
	}
	
	/*
	 * Tests Size method by adding 1 number and checking if the size of the list is 1. Then it adds
	 * another number and checks to see if the size is 2. Then it removes the previously added number
	 * and checks if the size is once again 1.
	 */
	@Test
	void testSize()
	{
		SimpleListV2 test = new SimpleListV2();
		test.add(1);
		assertEquals(test.size(), 1);
		test.add(2);
		assertEquals(test.size(), 2);
		test.remove(2);
		assertEquals(test.size(), 1);
	}
		

}
