/* February 27th 2020
 * Name: Dominik Czerny
 * ID: 1210724915
 * CSE 360
 * Assignment 2
 * SimpleList class creates methods to add, remove, search, count, append, first, last, size,
 *  and display contents of an array.
 */

package cse360assign2;

public class SimpleListV2 
{
	int[] list;
	int count;
	int max;
	
	
	//SimpleList constructor initializes the array to a size of 10 and count to 0
	SimpleListV2()
	{
		max = 10;
		list = new int[10];
		count = 0;
	}
	
	/*
	 * Add is a method that takes in a number to add into the first space of the array
	 * If the array hits the max number of slots, it will automatically add 50% more storage to
	 * the array.
	 */
	public void add(int number)
	{
		int tempArr [];
		int addSize;
		int hold;
		int next = number;
		
		if(count == max)
		{
			addSize = (max/2) + max;
			
			tempArr = list;
			
			list = new int[addSize];
			
			for(int iterate = 0; iterate < max; iterate++)
			{
				list[iterate] = tempArr[iterate];
			}
			
			max = addSize;
		}
		
		for(int iterate = 0; iterate < max; iterate++)
		{
			hold = list[iterate];
			list[iterate] = next;
			next = hold;
		}
		
		if(count != max)
		{
			count++;
		}
	}
	
	/*
	 * remove is a method that takes in a number and uses the search method to see if the number
	 * exists within the array. If it does the array will delete that number and push every number
	 * back one slot. The method will also check to see if more than 25% of the spaces are filled.
	 * If more than 25% is not filled it will reduce the size of the array by 25%.
	 */
	public void remove(int number)
	{	
		int tempArr[];
		int iterate = search(number);
		int removeSize;
		int emptySpace;
		
		if(iterate != -1)
		{
			while(iterate < max)
			{
				if(iterate + 1 == max)
				{
					list[iterate] = 0;
				}
				
				else
				{
					list[iterate] = list[iterate + 1];
				}
				
				iterate++;
			}
			
			count--;
			
			emptySpace = max - count;
			
			if(emptySpace > max*.25 && max > 1)
			{
				removeSize = (int) (max*.25);
				
				max -= removeSize;
				
				tempArr = list;
				
				list = new int[max];
				
				for(iterate = 0; iterate < max; iterate++)
				{
					list[iterate] = tempArr[iterate];
				}
			}
		}
	}
	
	/*
	 * Append takes input and appends the input to the end of list. If the list is full then
	 * the method allocates 50% more space to the list.
	 */
	public void append(int number)
	{
		int tempArr [];
		int addSize;
		int hold;
		int next = number;
		
		if(count == max)
		{
			addSize = (max/2) + max;
			
			tempArr = list;
			
			list = new int[addSize];
			
			for(int iterate = 0; iterate < max; iterate++)
			{
				list[iterate] = tempArr[iterate];
			}
			
			max = addSize;
		}
		
		list[count] = number;
		
		if(count != max)
		{
			count++;
		}
	}
	
	/*
	 * Count is a method that returns an int. It counts how many numbers are currently in the array.
	 */
	public int count()
	{
		return count;
	}
	
	public String toString()
	{
		String s = "";
		
		for(int iterate = 0; iterate < max; iterate++)
		{
			s += list[iterate] + " ";
		}
		return s;
	}
	
	/*
	 * Search is a method that takes in a number and searches to see if it is in the array.
	 * If the number is found the method returns the position, if not it returns -1
	 */
	public int search(int number)
	{
		int position = -1;
		
		for(int iterate = 0; iterate<max; iterate++)
		{
			if(list[iterate] == number)
			{
				 position = iterate;
			}
		}
		
		return position;
	}
	
	/*
	 * First returns the first integer in the list.
	 */
	public int first()
	{
		return list[0];
	}
	
	/*
	 * last returns the last integer in the list.
	 */
	public int last()
	{
		return list[count - 1];
	}
	
	/*
	 * size returns the size of the list not including the total space.
	 */
	public int size()
	{
		return count;
	}
}